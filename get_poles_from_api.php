<?php
require_once 'application/DatabaseManager.class.php';
if (isset($_POST['data1'])) {
	
	$query = "INSERT INTO pole(poleId, latitude, longitude, polename)
			  VALUES(:poleId, :latitude, :longitude, :polename)";

		$pole_id = $_POST['data1'];
		$latitude = $_POST['data2'];
		$longitude = $_POST['data3'];
		$polename = $_POST['data4'];

	$db = DatabaseManager::getDB();
	$stmt = $db->prepare($query);
	$stmt->bindParam(":poleId", $pole_id);
	$stmt->bindParam(":latitude", $latitude);
	$stmt->bindParam(":longitude", $longitude);
	$stmt->bindParam(":polename", $polename);

	$stmt->execute();
	echo json_encode($latitude);
}
?>