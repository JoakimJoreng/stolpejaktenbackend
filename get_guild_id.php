<?php
/**
*	Retrieves the guildId of the guild
*	related to @user_id
*	Echoes the guildId as JSON string
*/
require_once 'application/DatabaseManager.class.php';
if (isset($_GET['userId'])) {
	$db = DatabaseManager::getDB();

	$query = "SELECT guildId
			  FROM guild
		      JOIN team USING (guildId)
			  JOIN user USING (teamId)
			  WHERE userId = :userId";
	
	$user_id = $_GET['userId'];

	$stmt = $db->prepare($query);
	$stmt->bindParam(':userId', $user_id);                                   
	$stmt->execute();

	$result = array();
	while ($res = $stmt->fetch(PDO::FETCH_ASSOC)) {
	    $result[] = $res; 
	}
	header('Content-Type: application/json');
	if (count($result) != 0) {
		echo json_encode($result);
	} else {
		echo json_encode(array(array('guildId' => "0")));
	}
}	  
?>



