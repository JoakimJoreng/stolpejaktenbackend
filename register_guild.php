<?php 
require_once 'application/DatabaseManager.class.php';

if(isset($_GET['guildname']) && isset($_GET['guildleader'])) {
	$username = $_GET['guildleader'];
	$guild_name = $_GET['guildname'];
	$response = '';
	$guild_exist_query =  "SELECT guildId 
						   FROM guild 
						   WHERE guildName = :guild_name ";
	$db = DatabaseManager::getDB();
	$stmt = $db->prepare($guild_exist_query);
	$stmt->bindParam(':guild_name', $guild_name);
	$stmt->execute();
	$res = $stmt->fetch(PDO::FETCH_ASSOC);
	// If NOT guild is already registered
	if (!isset($res['guildId'])) {
		$user_in_guild_query = "SELECT guildId
								FROM guild 
						        JOIN team USING (guildId) 
								JOIN user USING (teamId) 
								WHERE userName = :userName";
		$stmt = $db->prepare($user_in_guild_query);
		$stmt->bindParam(':userName', $username);
		$stmt->execute();
		$res = $stmt->fetch(PDO::FETCH_ASSOC);
		// If NOT user is already part of a guild
		if (!isset($res['guildId'])) {
			$code = generateGuildCode($guild_name);
			$owner_user_id_query = "SELECT userId 
									FROM user 
									WHERE userName = :userName";
			$stmt = $db->prepare($owner_user_id_query);
			$stmt->bindParam(':userName', $username);
			$stmt->execute();
			$res = $stmt->fetch(PDO::FETCH_ASSOC);
			// If the user is a registered user in stolpejakten
			if (isset($res['userId'])) {
				createGuild($res['userId'], $guild_name, $code);
				$response = $code;
			} else {
				$response = "Dette er ikke en registrert bruker";
			}

		} else {
			$response = "Brukeren er allerede medlem av en virksomhet";
		}
	} else {
		$response = "Denne virksomheten eksisterer allerede";
	}

	echo $response;
}
	/*
	function that generates a code for the guild using
	the 4 first letters of the guildname and a random
	number between 10-99 
	
	@author Pål & Joakim
	@param guildName as String
	@return guildCode as String
	*/
	function generateGuildCode($guild_name) {
		$code_is_unique = false;
		$guild_code = "";

		while(!$code_is_unique) {
			$rand_number = rand(10, 99);
			$guild_name = substr($guild_name, 0, 4);
			$guild_code = $guild_name . $rand_number;
			// Retrieves guildId if the code is in use
			$code_exists_query = "SELECT guildId 
								  FROM guild 
								  WHERE guildCode = :guild_code";
			$db = DatabaseManager::getDB();					  	
			$stmt = $db->prepare($code_exists_query);
			$stmt->bindParam(':guild_code', $guild_code);
			$stmt->execute();
			$res = $stmt->fetch(PDO::FETCH_ASSOC);
			if (!isset($res['guildId'])) {
				$code_is_unique = true;
				return $guild_code;
			}
		}
	}

	function createGuild($owner_id, $guild_name, $guild_code) {
		$create_guild_query = "INSERT INTO guild(ownerUserId, guildName, guildCode) 
							   VALUES(:owner_user_id, :guild_name, :guild_code)";

		$db = DatabaseManager::getDB();					  	
		$stmt = $db->prepare($create_guild_query);
		$stmt->bindParam(':owner_user_id', $owner_id);
		$stmt->bindParam(':guild_name', $guild_name);
		$stmt->bindParam(':guild_code', $guild_code);
		$stmt->execute();

		$guild_id = $db->lastInsertId();
		$team_name = $guild_name . "#1";

		$create_team_query = "INSERT INTO team(guildId, teamName) 
							  VALUES(:guild_id, :team_name)";
		$stmt = $db->prepare($create_team_query);
		$stmt->bindParam(':guild_id', $guild_id);
		$stmt->bindParam(':team_name', $team_name);
		$stmt->execute();

		$team_id = $db->lastInsertId();

		$update_user_team_query = "UPDATE user 
								   SET teamId = :teamId 
							       WHERE userId = :owner_user_id";
		$stmt = $db->prepare($update_user_team_query);
		$stmt->bindParam(':teamId', $team_id);
		$stmt->bindParam(':owner_user_id', $owner_id);
		$stmt->execute();
	}
?>