<?php

/**
*	Creates a competition based on provided amount of days
*	Binds competition to @guild_id
*/
require_once 'application/DatabaseManager.class.php';
if (isset($_POST['data1']) && isset($_POST['data2'])) {
	$db = DatabaseManager::getDB();
	
	$guild_id_query = "SELECT guildId  
				       FROM guild 
				       JOIN team USING (guildId) 
				       JOIN user USING (teamId) 
				       WHERE userId = :user_id";
	
	$user_id = $_POST['data1'];
	$stmt = $db->prepare($guild_id_query);
	$stmt->bindParam(':user_id', $user_id);                                   
	$stmt->execute();
	$res = $stmt->fetch(PDO::FETCH_ASSOC);
	
	$guild_id = $res['guildId'];
	$days = $_POST['data2'];

	$competition_creation_query = "INSERT INTO competition(startDate, endDate, description, guildId)
								   VALUES(CURDATE(), CURDATE() + :days, 'dummyDescription', :guild_id)";
	$stmt = $db->prepare($competition_creation_query);
	$stmt->bindParam(':days', $days);
	$stmt->bindParam(':guild_id', $guild_id);                                   
	$stmt->execute();
}	  
?>