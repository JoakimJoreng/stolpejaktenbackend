<?php
require_once 'application/DatabaseManager.class.php';
if (isset($_POST['data'])) {
	$db = DatabaseManager::getDB();

	$query = "SELECT username
       	      FROM user 
       	      JOIN team USING (teamId) 
              WHERE teamId = (SELECT teamId 
                      	   	  FROM user
                      	  	  WHERE userId = :userId)";
	
	$user_id = $_POST['data'];

	$stmt = $db->prepare($query);
	$stmt->bindParam(':userId', $user_id);                                   
	$stmt->execute();

	$result = array();
	while ($res = $stmt->fetch(PDO::FETCH_ASSOC)) {
	    $result[] = $res; 
	}
	header('Content-Type: application/json');
	echo json_encode($result);
}	  
?>