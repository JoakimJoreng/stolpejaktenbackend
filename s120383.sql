/*
Navicat MySQL Data Transfer

Source Server         : stolpejakten
Source Server Version : 50173
Source Host           : mysql.stud.hig.no:3306
Source Database       : s120383

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2014-12-10 15:27:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for competition
-- ----------------------------
DROP TABLE IF EXISTS `competition`;
CREATE TABLE `competition` (
  `competitionId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `guildId` int(10) NOT NULL,
  PRIMARY KEY (`competitionId`,`guildId`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for guild
-- ----------------------------
DROP TABLE IF EXISTS `guild`;
CREATE TABLE `guild` (
  `guildId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ownerUserId` int(10) NOT NULL,
  `guildName` varchar(255) NOT NULL,
  `guildCode` varchar(255) NOT NULL,
  PRIMARY KEY (`guildId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pole
-- ----------------------------
DROP TABLE IF EXISTS `pole`;
CREATE TABLE `pole` (
  `poleId` int(10) unsigned NOT NULL,
  `latitude` double(255,6) NOT NULL,
  `longitude` double(255,6) NOT NULL,
  `polename` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`poleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for team
-- ----------------------------
DROP TABLE IF EXISTS `team`;
CREATE TABLE `team` (
  `teamId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `guildId` int(10) NOT NULL,
  `teamName` varchar(255) NOT NULL,
  PRIMARY KEY (`teamId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `teamId` int(10) unsigned DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `sId` int(10) NOT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for userpole
-- ----------------------------
DROP TABLE IF EXISTS `userpole`;
CREATE TABLE `userpole` (
  `userId` int(10) NOT NULL,
  `poleId` int(10) NOT NULL,
  `registrationDate` date NOT NULL,
  PRIMARY KEY (`userId`,`poleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
