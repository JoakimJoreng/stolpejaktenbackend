<?php
/**
*	Retrieves a full scorelist of all
*	the guilds in the database presented
*	with name and score
*
*	Echoed as JSON 
*/
require_once 'application/DatabaseManager.class.php';
$db = DatabaseManager::getDB();
$query = "SELECT guildName AS guild, COUNT(*) AS score
		  FROM guild 
		  JOIN team using (guildId) 
		  JOIN user USING (teamId) 
		  JOIN userpole USING (userId) 
		  GROUP BY guildName";

$stmt = $db->prepare($query);                               
$stmt->execute();

$result = array();
while ($res = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $result[] = $res; 
}
header('Content-Type: application/json');
echo json_encode($result);	  
?>