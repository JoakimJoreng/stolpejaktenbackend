<?php
/**
	Retrieves the username
	of all the users on the same team
	as the user with @user_id

	Echoes as JSON
*/
require_once 'application/DatabaseManager.class.php';
if (isset($_GET['userId'])) {
	$db = DatabaseManager::getDB();

	$query = "SELECT username
       	      FROM user 
       	      JOIN team USING (teamId) 
              WHERE teamId = (SELECT teamId 
                      	   	  FROM user
                      	  	  WHERE userId = :userId)";
	
	$user_id = $_GET['userId'];

	$stmt = $db->prepare($query);
	$stmt->bindParam(':userId', $user_id);                                   
	$stmt->execute();

	$result = array();
	while ($res = $stmt->fetch(PDO::FETCH_ASSOC)) {
	    $result[] = $res; 
	}
	header('Content-Type: application/json');
	echo json_encode($result);
}	  
?>