<?php
/**
*	Retrieves information about
*	the active competition in
*	the guild related to @user_id
*
*	Echos data as a JSON encoded string
*/
require_once 'application/DatabaseManager.class.php';
if (isset($_GET['userId'])) {
	$db = DatabaseManager::getDB();

	$query = "SELECT competition.*
			  FROM competition
			  JOIN guild USING (guildId)
			  JOIN team USING (guildId)
			  JOIN user USING (teamId)
			  WHERE userId = :user_id
			  AND CURDATE() >= startDate 
			  AND CURDATE() <= endDate";
	
	$user_id = $_GET['userId'];

	$stmt = $db->prepare($query);
	$stmt->bindParam(':user_id', $user_id);                                   
	$stmt->execute();

	$result = array();
	while ($res = $stmt->fetch(PDO::FETCH_ASSOC)) {
	    $result[] = $res; 
	}
	header('Content-Type: application/json');
	echo json_encode($result);
}	  
?>