<?php 
require_once 'application/DatabaseManager.class.php';

if (isset($_POST['guildname']) && isset($_POST['guildleader'])) {
	$username = $_POST['guildleader'];
	$guild_name = $_POST['guildname'];
	$response = '';

	$guild_exist_query =  "SELECT guildId
						   FROM guild
						   WHERE guildName = :guild_name";
	$db = DatabaseManager::getDB();
	$stmt = $db->prepare($guild_exist_query);
	$stmt->bindParam(':guild_name', $guild_name);
	$stmt->execute();
	$res = $stmt->fetch(PDO::FETCH_ASSOC);

	if (!isset($res['guildId'])) {
		$user_in_guild_query = "SELECT guildId
								FROM guild
						        JOIN team USING (guildId)
								JOIN user USING (teamId)
								WHERE userName = :userName";
		$stmt = $db->prepare($guild_exist_query);
		$stmt->bindParam(':username', $username);
		$stmt->execute();
		$res = $stmt->fetch(PDO::FETCH_ASSOC);
		if (!isset($res['guildId'])) {
			$response = generateGuildCode($guild_name);
		} else {
			$response = "Brukeren er allerede medlem av en virksomhet";
		}
	} else {
		$result = "Denne virksomheten eksisterer allerede";
	}

	echo $response;
}
	/*
	function that generates a code for the guild using
	the 4 first letters of the guildname and a random
	number between 10-99 
	
	@author Pål & Joakim
	@param guildName as String
	@return guildCode as String
	*/
	function generateGuildCode($guildName) {
		$codeIsUnique = false;
		$guildCode = "";
		
		while(!$codeIsUnique) {
			$randNumber = rand(10, 99);
			$guildName = substr($guildName, 0, 4);
			$guildCode = $guildName . $randNumber;
			// Retrieves guildId if the code is in use
			$code_exists_query = "SELECT guildId
								  FROM guild
								  WHERE guildCode = :guild_code";
			$stmt = $db->prepare($guild_exist_query);
			$stmt->bindParam(':guild_code', $guild_code);
			$stmt->execute();
			$res = $stmt->fetch(PDO::FETCH_ASSOC);
			if (!isset($res['guildId'])) {
				$codeIsUnique = true;
				return $guildCode;
			}
		}
	}
?>