/*
Navicat MySQL Data Transfer

Source Server         : testinghest
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : stolpejakten

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2014-12-07 12:50:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for competition
-- ----------------------------
DROP TABLE IF EXISTS `competition`;
CREATE TABLE `competition` (
  `competitionId` int(10) NOT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `guildId` int(10) NOT NULL,
  PRIMARY KEY (`competitionId`,`guildId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for guild
-- ----------------------------
DROP TABLE IF EXISTS `guild`;
CREATE TABLE `guild` (
  `guildId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ownerUserId` int(10) NOT NULL,
  PRIMARY KEY (`guildId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pole
-- ----------------------------
DROP TABLE IF EXISTS `pole`;
CREATE TABLE `pole` (
  `poleId` int(10) unsigned NOT NULL,
  `latitude` double(255,0) NOT NULL,
  `longitude` double(255,0) NOT NULL,
  PRIMARY KEY (`poleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for team
-- ----------------------------
DROP TABLE IF EXISTS `team`;
CREATE TABLE `team` (
  `teamId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `guildId` int(10) NOT NULL,
  PRIMARY KEY (`teamId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `teamId` int(10) unsigned NOT NULL,
  `username` varchar(255) NOT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for userpole
-- ----------------------------
DROP TABLE IF EXISTS `userpole`;
CREATE TABLE `userpole` (
  `userId` int(10) NOT NULL,
  `poleId` int(10) NOT NULL,
  `registrationDate` date NOT NULL,
  PRIMARY KEY (`userId`,`poleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
