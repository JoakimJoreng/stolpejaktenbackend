<?php
/**
*	Retrieves the userId based on the received
*	@stolpejakten_id
*	
*	If the received @stolpejakten_id
*	is unknown to the database a new user
*	will be created and bind the the @stolpejakten_id
*
*	Echoes the @userId as JSON
*/
require_once 'application/DatabaseManager.class.php';
if (isset($_GET['stolpejaktenId'])) {
	$db = DatabaseManager::getDB();

	$query = "SELECT userId
       	      FROM user 
              WHERE sId = :stolpejakten_id";
	
	$stolpejakten_id = $_GET['stolpejaktenId'];

	$stmt = $db->prepare($query);
	$stmt->bindParam(':stolpejakten_id', $stolpejakten_id);                                   
	$stmt->execute();

	$res = $stmt->fetch(PDO::FETCH_ASSOC);

	if (isset($res['userId'])) {
		header('Content-Type: application/json');
		echo json_encode(array($res));
	} else {
		$username = $_GET['username'];
		$new_user_query = "INSERT INTO user(username, sId) 
						   VALUES(:username, :stolpejakten_id)";
		$stmt = $db->prepare($new_user_query);
		$stmt->bindParam(':username', $username);
		$stmt->bindParam(':stolpejakten_id', $stolpejakten_id);                                   
		$stmt->execute();

		header('Content-Type: application/json');
		echo json_encode(array(array('userId' => $db->lastInsertId())));
	}
}	  
?>