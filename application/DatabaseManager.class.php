<?php
require_once('StolpejaktenConfig.class.php');
/**
 * Manages and configures the PDO object used for database interaction
 */
class DatabaseManager
{
    /**
     * The PDO instance
     * @var PDO
     */
    private static $pdo = NULL;
    
    /**
     * Creates (if needed) the PDO object for the database host
     * @return mixed
     */
    public static function getDB()
    {
        if (self::$pdo == NULL)
        {
            self::$pdo = new PDO('mysql:host=' . StolpejaktenConfig::DB_HOST . ';dbname=' . StolpejaktenConfig::DB_NAME . ';charset=utf8',
                                 StolpejaktenConfig::DB_USER, StolpejaktenConfig::DB_PSSW);
            self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        
        return self::$pdo;
    }
}
?>