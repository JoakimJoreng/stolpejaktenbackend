<?php
class StolpejaktenConfig
{
    /**
     * The host-name used for connecting to the databse. Can also be IP-address.
     */
    const DB_HOST = 'mysql.stud.hig.no';
    
    /**
     * Username used when connecting to the database
     */
    const DB_NAME = 's120383';
    
    /**
     * Name of the database
     */
    const DB_USER = 's120383';
    
    /**
     * Super secret database password
     */
    const DB_PSSW = '';
}
?>