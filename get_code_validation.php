<?php
/**
*	Binds a @user_id to the default team
*	in the guild related to the 
*	provided code @user_code 
*
*	Echoes the guildname as JSON string
*/
require_once 'application/DatabaseManager.class.php';

if (isset($_GET['guildCode']) && isset($_GET['userId'])) {
	$db = DatabaseManager::getDB();
	$code_exists_query = "SELECT guildName, guildId
						  FROM guild
						  WHERE guildCode = :guild_code";
	$guild_code = $_GET['guildCode'];
	$user_id = $_GET['userId'];

	$stmt = $db->prepare($code_exists_query);
	$stmt->bindParam(':guild_code', $guild_code);
	$stmt->execute();
	$res = $stmt->fetch(PDO::FETCH_ASSOC);

	if (isset($res['guildName']) && isset($res['guildId'])) {
		$guild_id = $res['guildId'];
		$bind_user_guild_query = "UPDATE user 
								  SET teamId = (SELECT teamId 
								  				FROM team 
								  				WHERE guildId = :guild_id 
								  				LIMIT 1) 
								  WHERE userId = :user_id";
		$stmt = $db->prepare($bind_user_guild_query);
		$stmt->bindParam(':guild_id', $guild_id);
		$stmt->bindParam(':user_id', $user_id);
		$stmt->execute();					  

		header('Content-Type: application/json');							  
		echo json_encode(array($res));
	} else {
		header('Content-Type: application/json');
		echo json_encode(array(array('guildName' => 0)));
	}
}
?>